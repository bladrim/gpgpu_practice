#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define CL_TARGET_OPENCL_VERSION 120
#include "ocl_boiler.h"

//bruuuuttto
size_t gws_align_init;
size_t gws_align_smooth;

cl_event vecinit(cl_kernel vecinit_k, cl_command_queue que, 
        cl_mem d_v1, cl_int nels)
{
    const size_t gws[] = { round_mul_up(nels, gws_align_init) };
    printf("init gws: %d | %zu = %zu\n", nels, gws_align_init, gws[0]);
    cl_event vecinit_evt;
    cl_int err;

    cl_uint i = 0;
    err = clSetKernelArg(vecinit_k, i++, sizeof(d_v1), &d_v1);
    ocl_check(err, "set vecinit arg", i-1);
    err = clSetKernelArg(vecinit_k, i++, sizeof(nels), &nels);
    ocl_check(err, "set vecinit arg", i-1);

    err = clEnqueueNDRangeKernel(que, vecinit_k, 1,
            NULL, gws, NULL,
            0, NULL, &vecinit_evt); 
    ocl_check(err, "enqueue vecinit");

    return vecinit_evt;
}

cl_event vecsmooth(cl_kernel vecsmooth_k, cl_command_queue que, 
        cl_mem d_vsmooth, cl_mem d_v1, 
        cl_int nels, cl_event init_evt)
{
    const size_t gws[] = { round_mul_up(nels, gws_align_smooth) };
    printf("smooth gws: %d | %zu = %zu\n", nels, gws_align_smooth, gws[0]);
    cl_event vecsmooth_evt;
    cl_int err;

    cl_uint i = 0;
    err = clSetKernelArg(vecsmooth_k, i++, sizeof(d_vsmooth), &d_vsmooth);
    ocl_check(err, "set vecsmooth arg", i-1);
    err = clSetKernelArg(vecsmooth_k, i++, sizeof(d_v1), &d_v1);
    ocl_check(err, "set vecsmooth arg", i-1);
    err = clSetKernelArg(vecsmooth_k, i++, sizeof(nels), &nels);
    ocl_check(err, "set vecsmooth arg", i-1);

    err = clEnqueueNDRangeKernel(que, vecsmooth_k, 1,
            NULL, gws, NULL,
            1, &init_evt, &vecsmooth_evt); 

    ocl_check(err, "enqueue vecsmooth");

    return vecsmooth_evt;
}

void verify(const int *vsmooth, int nels)
{
    for (int i =0; i < nels; ++i){
        int target = i == nels -1 ? i -1 : i;
        if (vsmooth[i] != target){
            fprintf(stderr, "mismatch @ %d : %d != %d\n",
                    i, vsmooth[i], target);
            exit(1);
        }
    }
} 

int main(int argc, char *argv[])
{
    if (argc <= 1) {
        fprintf(stderr, "specify number of elements\n");
        exit(1);
    }

    const int nels = atoi(argv[1]);
    const size_t memsize = nels*sizeof(cl_int);
    
    cl_platform_id p = select_platform();
    cl_device_id d = select_device(p);
    cl_context ctx = create_context(p, d);
    cl_command_queue que = create_queue(ctx, d);
    cl_program prog = create_program("vecsmooth.cl", ctx, d);
    cl_int err;
    
    cl_kernel vecinit_k = clCreateKernel(prog, "vecinit", &err);
    ocl_check(err, "create kernel vecinit");
    cl_kernel vecsmooth_k  = clCreateKernel(prog, "vecsmooth", &err);
    ocl_check(err, "create kernel vecsmooth");

    
    /*get information about work-group size of device */
    err = clGetKernelWorkGroupInfo(vecinit_k, d,
            CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
            sizeof(gws_align_init), &gws_align_init, NULL);
    ocl_check(err, "preferred wg multiple for init");
    err = clGetKernelWorkGroupInfo(vecsmooth_k, d,
            CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
            sizeof(gws_align_smooth), &gws_align_smooth, NULL);
    ocl_check(err, "preferred wg multiple for smooth");
    // int haven't  a constant size so we should
    // use static dimensions variables.
    //int32_t *v1= NULL, *v2= NULL, *vsmooth=NULL;
    // for sake of semplicity we'll start just with int
    cl_mem d_v1 = NULL, d_vsmooth = NULL;
    
    // In c a void pointer is upgraded to a whatever pointer
    // in c++ we should cast the return of malloc to a int pointer.
    // size_t is an integer type but is big enough to contain
    // the biggest number for the hw architecture.
    // everytime we allocate memory we should check 
    // the success of the process.
    d_v1 = clCreateBuffer(ctx, 
            CL_MEM_READ_WRITE | CL_MEM_HOST_NO_ACCESS,
            memsize, NULL,
            &err);
    ocl_check(err, "create buffer d_v1");
    
    d_vsmooth = clCreateBuffer(ctx, 
            CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
            memsize, NULL,
            &err);
    ocl_check(err, "create buffer d_vsmooth");

    cl_event init_evt, smooth_evt, read_evt;

    init_evt = vecinit(vecinit_k, que, d_v1, nels);

    smooth_evt = vecsmooth(vecsmooth_k, que, d_vsmooth, d_v1, nels, init_evt); 

    cl_int *h_vsmooth = clEnqueueMapBuffer(que, d_vsmooth, CL_FALSE,
            CL_MAP_READ,
            0, memsize, 
            1, &smooth_evt, &read_evt, &err);

#if 0
    clFinish(que);
    //Controlla che l'esecuzione sia conclusa
#else
    clWaitForEvents(1, &read_evt);
#endif
    //clFlush(que); 
    //simile a clFinish ma non aspetta la fine dell'esecuzione ma la 
    //recezione degli eventi da parte della gpu

    

    verify(h_vsmooth, nels);

    const double runtime_init_ms = runtime_ms(init_evt); 
    const double runtime_smooth_ms  = runtime_ms(smooth_evt);
    const double runtime_read_ms = runtime_ms(read_evt);

    const double init_bw_gbs = 2.0*memsize/1.0e6/runtime_init_ms;
    const double smooth_bw_gbs = 3.0*memsize/1.0e6/runtime_smooth_ms;
    const double read_bw_gbs = memsize/1.0e6/runtime_smooth_ms;

    printf("init: %d int in %gms %g GB/s %g GE/s\n", 
            nels, runtime_init_ms, init_bw_gbs, 
            nels/1.0e6/runtime_init_ms); 
    printf("smooth : %d int in %gms %g GB/s %g GE/s\n",
            nels, runtime_smooth_ms, smooth_bw_gbs, 
            nels/1.0e6/runtime_smooth_ms);
    printf("read: %d int in %gms %g GB/s %g GE/s\n", 
            nels, runtime_read_ms, read_bw_gbs, 
            nels/1.0e6/runtime_read_ms);
    // v1 = NULL to avoid double free: if the pointer is null it doesn't free
    // the memory again
    
    err = clEnqueueUnmapMemObject(que, d_vsmooth, h_vsmooth,
            0, NULL, NULL);
    ocl_check(err, "unmap h_vsmooth");

    clReleaseMemObject(d_vsmooth);
    clReleaseMemObject(d_v1);
    clReleaseKernel(vecsmooth_k);
    clReleaseKernel(vecinit_k);
    clReleaseProgram(prog);
    clReleaseCommandQueue(que);
    clReleaseContext(ctx);
}
