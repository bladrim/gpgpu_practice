kernel void vecinit(
    global int * restrict v1, 
    int nels)
{
    const int i = get_global_id(0); 

    if(i >= nels) return;

    v1[i] = i;
}

kernel void vecsmooth(
    global int * restrict s,
    global const int * restrict v,
    int nels)
{
    const int i = get_global_id(0); 

    if(i >= nels) return;
   
#if 0
    if(nels == 1)
        s[i] = v[1];
    else if (i == 0)
        s[i] = (v[i] + v[i+1])/2;
    else if (i == nels - 1)
        s[i] = (v[i] + v[i-1])/2;
    else
        s[i] = (v[i-1] + v[i] + v[i+1])/3;
#else 
    int v1 = 0, v2 = v[i], v3 = 0;
    int c = 1;
    if(i > 0) {
        v1 = v[i-1];
        ++c;
    }
    if(i + 1 < nels) {
        v3 = v[i+1];
        ++c;
    }
    s[i] = (v1+v2+v3)/c;
#endif
}
