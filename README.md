# GPGPU Course

All the code has been written while following the GPGPU course and the
material given by the professor.

I wasn't able to find much about Opencl that managed to guide me through the
awful lot of nuances of the its API.
So this repo is my "homework" archive and a way to point at some good 
learning material.

I'm just sorry the videos are in italian.

University of Catania Computer Science department: [GPGPU 2020 Course](https://www.youtube.com/watch?v=Aug3HzGtP6Q&list=PLwUevCfXfEAHaHJsQ2fcd3Le_inr6NvLM)

