kernel void vecinit(
    global int * restrict v1, 
    global int * restrict v2,
    int nels)
{
    const int i = get_global_id(0); 

    if(i >= nels) return;

    v1[i] = i;
    v2[i] = nels -i;
}

kernel void vecsum(
    global int * restrict vsum,
    global const int * restrict v1,
    global const int * restrict v2,
    int nels)
{
    const int i = get_global_id(0); 

    if(i >= nels) return;

    vsum[i] = v1[i] + v2[i];
}

kernel void vecsum4(
    global int4 * restrict vsum,
    global const int4 * restrict v1,
    global const int4 * restrict v2,
    int nquarts)
{
    const int i = get_global_id(0); 

    if(i >= nquarts) return;

    vsum[i] = v1[i] + v2[i];
}

kernel void vecsum8(
    global int8 * restrict vsum,
    global const int8 * restrict v1,
    global const int8 * restrict v2,
    int noct)
{ 
    const int i = get_global_id(0); 

    if(i >= noct) return;

    vsum[i] = v1[i] + v2[i];
}

kernel void vecsum16(
    global int16 * restrict vsum,
    global const int16 * restrict v1,
    global const int16 * restrict v2,
    int nhex)
{ 
    const int i = get_global_id(0); 

    if(i >= nhex) return;

    vsum[i] = v1[i] + v2[i];
}


kernel void vecsum4x4(
    global int4 * restrict vsum,
    global const int4 * restrict v1,
    global const int4 * restrict v2,
    int nhex)
{ 
    const int i = get_global_id(0); 
    const int gws = get_global_size(0);

    if(i >= nhex) return;

    int4 a0 = v1[i + 0*gws];
    int4 a1 = v1[i + 1*gws];
    int4 a2 = v1[i + 2*gws];
    int4 a3 = v1[i + 3*gws];

    int4 b0 = v2[i + 0*gws];
    int4 b1 = v2[i + 1*gws];
    int4 b2 = v2[i + 2*gws];
    int4 b3 = v2[i + 3*gws];
    
    a0 += b0;
    a1 += b1;
    a2 += b2;
    a3 += b3;

    vsum[i + 0*gws] = a0;
    vsum[i + 1*gws] = a1;
    vsum[i + 2*gws] = a2;
    vsum[i + 3*gws] = a3;
}

kernel void vecsum4x2(
    global int4 * restrict vsum,
    global const int4 * restrict v1,
    global const int4 * restrict v2,
    int noct)
{ 
    const int i = get_global_id(0); 
    const int gws = get_global_size(0);

    if(i >= noct) return;

    int4 a0 = v1[i + 0*gws];
    int4 a1 = v1[i + 1*gws];

    int4 b0 = v2[i + 0*gws];
    int4 b1 = v2[i + 1*gws];
    
    a0 += b0;
    a1 += b1;

    vsum[i + 0*gws] = a0;
    vsum[i + 1*gws] = a1;
}
